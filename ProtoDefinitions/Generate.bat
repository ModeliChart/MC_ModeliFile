REM It is assumed that protoc.exe is in PATH or the current directory
REM The output directory must exis before generating the code
mkdir CSharp
mkdir Cpp
REM Each file needs to be compiled seperately
.\protoc Channel.proto --csharp_out=CSharp --cpp_out=Cpp
.\protoc SimulationSettings.proto --csharp_out=CSharp --cpp_out=Cpp
.\protoc UiSettings.proto --csharp_out=CSharp --cpp_out=Cpp
pause