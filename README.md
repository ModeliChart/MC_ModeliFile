# About Modeli files

A Modeli file is a zip archive.

## Structure

    /top level
  
        /.SimulationProto
  
        /.UiProto
  
        /Settables.txt
    
        /{model GUID}
  
## Contents

* .SimulationProto is based on SimulationSettings.proto
* .UiProto is based on UiSettings.proto
* Settables.txt is a parameter file for model variables. It is named 
Settables.txt for legacy support.
* {model GUI} is a directory which contains the unzipped fmu for this GUI. The
models must match the ones specified in the SimulationSettings.

# Usage

A generator batch script is given for C++ and C#. You must provide the protoc
executable that suits your setup (compile the source or get it via a package 
manager).